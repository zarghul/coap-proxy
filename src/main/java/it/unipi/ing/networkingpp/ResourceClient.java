package it.unipi.ing.networkingpp;

import org.eclipse.californium.core.CoapClient;
import org.eclipse.californium.core.CoapHandler;

import org.eclipse.californium.core.coap.OptionNumberRegistry;

import org.eclipse.californium.core.coap.Request;
import org.eclipse.californium.core.coap.Response;
import org.eclipse.californium.core.CoapObserveRelation;
import org.eclipse.californium.core.CoapResponse;

import org.eclipse.californium.core.coap.CoAP.Code;
import org.eclipse.californium.core.coap.CoAP.ResponseCode;

import org.eclipse.californium.core.coap.OptionSet;
import org.eclipse.californium.core.coap.Option;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.Cache;

import java.util.List;
import java.util.LinkedList;
import java.util.Iterator;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import java.util.logging.Level;
import java.util.logging.Logger;

public class ResourceClient extends CoapClient{
	private static final Logger LOGGER = Logger.getLogger(ResourceClient.class.getCanonicalName());

	private final Cache<CacheKey,CachedResponse> responseCache;
	private ExecutorService executor;
	private Timer timer;

	private class CacheKey {
		private List<Option> options;

		public CacheKey(Request request){
			request.getOptions().removeObserve();
			options = new LinkedList<Option>();

			for(Option o : request.getOptions().asSortedList()){
				if(o == null){
					continue;
				}
				if(o.isNoCacheKey()){
					continue;
				}
				if(o.getNumber()==OptionNumberRegistry.URI_PATH){
					continue;
				}
				if(o.getNumber()==OptionNumberRegistry.BLOCK2){
					System.out.println(o.toString());
					continue;
				}
				options.add(o);	
			}
		}
		@Override
		public boolean equals(Object obj){
			if (this == obj) {
				return true;
			}
			if (obj == null) {
				return false;
			}
			if (getClass() != obj.getClass()) {
				return false;
			}
			return options.equals(((CacheKey)obj).options);
		}

		@Override
		public int hashCode(){
			int hash = 1;
			for(Option o : options){
				hash = 31*hash + (o.getNumber()*31+java.util.Arrays.hashCode(o.getValue()));
			}
			return hash;
		}
	}
	private class CachedResponse {
		private long maxAgeNano = 0;
		private Response response;
		public CachedResponse(Response response){
			this.response = response;

			Long maxAgeSeconds = response.getOptions().getMaxAge();
			if(maxAgeSeconds == null){
				maxAgeSeconds = new Long(60);
			}
			maxAgeNano = 1000000000*maxAgeSeconds;

		}

		public boolean isFresh(){
		
			long arriveTime = response.getTimestamp();
			long currentTime = System.nanoTime();
			long cacheTime = currentTime-arriveTime;
			long newMaxAge = maxAgeNano - cacheTime;

			if(newMaxAge/1000000000 <= 0){
				return false;
			}

			return true;
		}

		public Response get(){
			if(!isFresh()){
				return null;
			}

			long arriveTime = response.getTimestamp();
			long currentTime = System.nanoTime();
			long cacheTime = currentTime-arriveTime;
			long newMaxAge = maxAgeNano - cacheTime;

			Response resp = copyResponse(response);
			resp.getOptions().setMaxAge(newMaxAge/1000000000);

			return resp;

		}
	}

	/**
	 * utility function for copying {@link Response} objects.
	 * It copies only meaningful options
	 * @param orig	original {@link Response} object to copy. 
	 */
	private Response copyResponse(Response orig){
		
		Response copy = new Response(orig.getCode());
		copy.setPayload(orig.getPayload());
		copy.setTimestamp(orig.getTimestamp());

		OptionSet opts = new OptionSet(orig.getOptions());
		
		opts.removeBlock1();
		opts.removeBlock2();
		opts.clearUriPath();

		copy.setOptions(opts);


		return copy;
	}
	/**
	 * utility function for copying {@link Request} objects.
	 * It copies only meaningful options
	 * @param orig	original {@link Request} object to copy. 
	 */
	private Request translateRequest(Request orig){
		
		Request copy = new Request(orig.getCode());
		copy.setPayload(orig.getPayload());
		copy.setTimestamp(System.nanoTime());
		OptionSet opts = new OptionSet(orig.getOptions());
		
		opts.removeBlock1();
		opts.removeBlock2();
		opts.clearUriPath();

		copy.setOptions(opts);
		copy.setURI(getURI());

		return copy;
	}


	public ResourceClient(String URI){
		super(URI);

		setTimeout(0);

		responseCache = CacheBuilder.newBuilder().recordStats().build();

		executor = Executors.newCachedThreadPool();
		timer = new Timer();
	}


	private Response askForResponse(Request request){
		LOGGER.log(Level.INFO,"Asking real resource...");
		CoapResponse cr =  advanced(request);
		Response r = null;
		if(cr==null){
			//L'endpoint non risponde,mandiamo GATEWAY_TIMEOUT
			r = new  Response(ResponseCode.GATEWAY_TIMEOUT);
				
		}
		else {
			r = cr.advanced();
		}
		r.setTimestamp(System.nanoTime());
		LOGGER.log(Level.INFO,"Resource answered!");

		return r;
	}

	public Response handleRequest(Request request){	
		//System.out.println("cachedGet");
		//Thread.dumpStack();
		
		final Request req = translateRequest(request);
		try{
			Response res = null;
			if(req.getCode()==Code.GET){
				CacheKey key = new CacheKey(req);
				//TODO prova solo un certo numero di volte
				while(res == null){
					res = responseCache.get(key,new Callable<CachedResponse>(){
						@Override
						public CachedResponse call(){
							LOGGER.log(Level.INFO,"Cache miss, asking to resource...");
							Response r = askForResponse(req);
							return new CachedResponse(r);
						}
					}).get();

					if(res == null){
						LOGGER.log(Level.INFO,"Response not fresh! Invalidate cache entry");
						responseCache.invalidate(key);
					}
					else if(!ResponseCode.isSuccess(res.getCode())){
						responseCache.invalidate(key);	
						System.out.println("not success");
					}
					else{
					//	System.out.println("cachedGet: "+res.getPayloadString());
					}
				};

				if(res.getCode()!=ResponseCode.CONTENT){
					LOGGER.log(Level.INFO,"Response is not cacheable");
					responseCache.invalidate(key);
				}
			}
			else {
				res = askForResponse(req);
				if(res.getCode()==ResponseCode.CHANGED){
					responseCache.invalidateAll();
					LOGGER.log(Level.INFO,"Response code is 2.04 CHANGED. Cache invalidated!");
				}
			}
			return copyResponse(res);
		}catch(ExecutionException e){
			System.err.println(e.getCause());
			return new Response(ResponseCode.INTERNAL_SERVER_ERROR);
		}
	}

	public Future<?> observeOrPoll(final Request request, final NotificationHandler handler){
		final CacheKey key = new CacheKey(request);

		return executor.submit(new Runnable() {
			@Override
			public void run() {
				LOGGER.log(Level.INFO,"Sending observe request to Resource...");
				Request req = translateRequest(request);
				req.getOptions().setObserve(0);
				Response res = advanced(req).advanced();

				Boolean polling = null;
				try{
					while  (true){
						if(!res.getOptions().hasObserve()){
							if(polling == null || polling == false){
								LOGGER.log(Level.INFO,"Observing not available. Falling back to polling...");
								polling = true;
							}
							req  = translateRequest(request);
							req.getOptions().setObserve(0);
							final Request  tReq = req;

							timer.schedule(new TimerTask(){
								@Override
								public void run() {
									send(tReq);
								}
							},TimeUnit.SECONDS.toMillis(res.getOptions().getMaxAge()));
						}else{
							if(polling == null || polling == true){
								LOGGER.log(Level.INFO,"Observing relation accepted!");
								polling = false;
							}
						}
						res = req.waitForResponse();
						if(res==null){
							res = new  Response(ResponseCode.NOT_FOUND);
						}
						//System.out.println("polling result: "+res.getPayloadString());
						res.setTimestamp(System.nanoTime());
						responseCache.put(key,new CachedResponse(copyResponse(res)));
						handler.onReceived();

						if(Thread.interrupted()){
								LOGGER.log(Level.INFO,"Stop task");
								throw new InterruptedException();	
						}

					}

				}catch (InterruptedException e){
					if(res.getOptions().hasObserve()){
						LOGGER.log(Level.INFO,"Stop observing");
						Request cancel = Request.newGet();
						cancel.setOptions(req.getOptions());
						cancel.setObserveCancel();
						cancel.setToken(req.getToken());
						cancel.setDestination(req.getDestination());
						cancel.setDestinationPort(req.getDestinationPort());
						advanced(cancel);
						req.cancel();

					}else{
						LOGGER.log(Level.INFO,"Stop polling");
					}
				}catch (Exception e){
					e.printStackTrace();
					handler.onError();
				}
				LOGGER.log(Level.INFO,"Exiting from observing task");
			}
		});
	}
}
