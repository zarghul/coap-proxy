package it.unipi.ing.networkingpp;

import org.eclipse.californium.core.server.resources.DiscoveryResource;
import org.eclipse.californium.core.server.resources.Resource;
import org.eclipse.californium.core.server.resources.CoapExchange;

import org.eclipse.californium.core.CoapClient;
import org.eclipse.californium.core.coap.CoAP.ResponseCode;
import org.eclipse.californium.core.CoapResource;
import org.eclipse.californium.core.WebLink;
import org.eclipse.californium.core.server.resources.CoapExchange;

import java.util.Arrays;
import java.util.Set;
import java.util.List;
import java.util.ArrayList;
import java.util.LinkedList;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.logging.Level;
import java.util.logging.Logger;

public class ResourceDiscovery extends DiscoveryResource {
	
	private static final Logger LOGGER = Logger.getLogger(ResourceDiscovery.class.getCanonicalName());

	private Resource root;
	private CoapClient client;
	private List<String> resources;

	public ResourceDiscovery(Resource root,String URI) {
		super(root);

		this.root = root;
		this.client = createClient(URI);

		this.resources = new LinkedList<String>();
	}

	public boolean discover(){
		Set<WebLink> response = client.discover();
		if(response == null){
			// Server did not answer.
			return false;
		}
		List<String> newResources = new LinkedList<String>();
		for(WebLink w: response){
			newResources.add(w.getURI());
		}

		List<String> toAdd = new LinkedList<String>(newResources);
		toAdd.removeAll(resources);

		List<String> toRemove = new LinkedList<String>(resources);
		toRemove.removeAll(newResources);

		for(String w : toAdd){
			if(w.equals("/.well-known/core")){
				continue;
			}
			LOGGER.info("new resource: "+w);
			try{
				URI uri = new URI(w);	
				List<String> path = new ArrayList(Arrays.asList(uri.getPath().split("/")));
				path.remove(path.get(0));

				String elem = path.get(path.size()-1);
				path.remove(elem);
				Resource current = root;
				for(String p : path){
					Resource c = current.getChild(p);
					if(c == null){
						c = new CoapResource(p);
						current.add(c);
					}
					current = c;
				}
				current.add(new ReverseProxyResource(elem,client.getURI()+uri.toString()));

			}catch(URISyntaxException e){
				e.printStackTrace();
			}
		}

		for(String w : toRemove){
			if(w.equals("/.well-known/core")){
				continue;
			}
			try{
				LOGGER.info("deleted resource: "+w);
				URI uri = new URI(w);	
				List<String> path = new ArrayList(Arrays.asList(uri.getPath().split("/")));
				path.remove(path.get(0));

				String elem = path.get(path.size()-1);
				path.remove(elem);
				CoapResource current = this;
				for(String p : path){
					CoapResource c = (CoapResource)current.getChild(p);
					if(c==null){
						continue;
					}
					current = c;
				}
				current.remove(elem);
				while(current!=this){
					if(current.getChildren().size()==0){
						CoapResource parent = (CoapResource)current.getParent();
						parent.remove(current);
						current = parent;
					}
					else{
						break;
					}
				}
			}catch(URISyntaxException e){
			}
		}

		resources = newResources;
		return true;
	}

	@Override 
	public void handleGET(CoapExchange exchange){
		boolean success = discover();
		if(success){
			super.handleGET(exchange);	
		}
		else{
			exchange.respond(ResponseCode.NOT_FOUND);
		}
	}
}
