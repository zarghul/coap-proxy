package it.unipi.ing.networkingpp;

import org.eclipse.californium.core.CoapClient;
import org.eclipse.californium.core.CoapHandler;

import org.eclipse.californium.core.coap.Request;
import org.eclipse.californium.core.coap.Response;
import org.eclipse.californium.core.CoapObserveRelation;
import org.eclipse.californium.core.CoapResponse;

import org.eclipse.californium.core.observe.ObserveRelation;

import org.eclipse.californium.core.server.resources.ConcurrentCoapResource;
import org.eclipse.californium.core.server.resources.CoapExchange;
import org.eclipse.californium.core.coap.CoAP.Code;
import org.eclipse.californium.core.coap.CoAP.ResponseCode;
import org.eclipse.californium.core.coap.CoAP.Type;
import org.eclipse.californium.core.coap.OptionSet;
import org.eclipse.californium.core.coap.Option;
 
import org.eclipse.californium.core.network.Exchange;

import java.util.List;
import java.util.LinkedList;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Reverse proxy resurce class.
 * This class represents a resource installed on the poxy that behaves like the
 * original resource from a client point ov view.
 */
public class ReverseProxyResource extends ConcurrentCoapResource {

	private static final Logger LOGGER = Logger.getLogger(ReverseProxyResource.class.getCanonicalName());

	private ResourceClient resClient; 
	private Map<String,ObserveMapping> observeStore;

	/**
	 * Class constructor.
	 * @param name	resource name. 
	 */
	public ReverseProxyResource(String name,String URI) {
		//call parent constructor
		super(name);

		//set observable
		setObservable(true); // enable observing
		setObserveType(Type.CON); // configure the notification type to CONs
		getAttributes().setObservable(); // mark observable in the Link-Format

		//client object to communicate with original resource
		resClient = new ResourceClient(URI);
		
		observeStore = new ConcurrentHashMap<String,ObserveMapping>();
	}

	/**
	 * This function is called after {@link handleRequest}.
	 * The parent version set the observe relation with the client if needed.
	 * This version also set the relation with the real resource if not yet
	 * established.
	 * @param exchange	the {@link Exchange} object related to the request
	 * @param response	the {@link Response} object produced by the 
	 * 					{@link handleRequest} call
	 */
	@Override
	public void checkObserveRelation(final Exchange exchange, Response response){

		//if client request observing and observe relation with resource not 
		//established yet, do it.
		final ObserveRelation rel = exchange.getRelation(); 
		if(rel!= null && !rel.isEstablished() &&
				ResponseCode.isSuccess(response.getCode())){
			
			final String query = exchange.getRequest().getOptions().getUriQueryString();
			LOGGER.info("Requested observe relation with query string: "+query);
			ObserveMapping map = observeStore.get(query);
			if(map==null){
				LOGGER.info("establish new observe relation with resource");			
				Future<?> task = resClient.observeOrPoll(exchange.getRequest(),
						new NotificationHandler(){
					@Override
					public void onReceived(){
						changed();
					}
					@Override
					public void onError(){
						List<ObserveRelation> endpointObservers = observeStore.get(query).
							endpointObservers;
						for(ObserveRelation o : endpointObservers){
							if(o.isEstablished()){
								o.cancel();
							}
						}
						observeStore.remove(query);
					}
				});	
				List<ObserveRelation> endpointObservers = new LinkedList<ObserveRelation>();
				endpointObservers.add(rel);
				map = new ObserveMapping(endpointObservers,task);
				observeStore.put(query,map);
			}else{
				LOGGER.info("using existing observe relation");			
				map.endpointObservers.add(rel);
			}
		}
		//call parent function
		super.checkObserveRelation(exchange,response);
	}

	@Override
	public void removeObserveRelation(ObserveRelation relation){
		
		for(String query : observeStore.keySet()){
			ObserveMapping om = observeStore.get(query);
			List<ObserveRelation> endpointObservers = om.endpointObservers;
			if(endpointObservers.remove(relation)){
				if(endpointObservers.size()==0){
					om.observeResourceTask.cancel(true);
					observeStore.remove(query);	
					LOGGER.log(Level.INFO,"No more client interested. Stopping resource observing..");
				}
				break;
			}
		}
		
		super.removeObserveRelation(relation);
	}

	@Override
	public void delete(){
		LOGGER.log(Level.INFO,"Deleting ReverseProxyResource "+getName());
		for(String query : observeStore.keySet()){
			ObserveMapping om = observeStore.get(query);
			om.observeResourceTask.cancel(true);
		}

		super.delete();
	}

	/**
	 * This method is called for every request to this resource.
	 * @param exchange	the {@link Exchange} object of the request
	 */
	@Override
	public void handleRequest(final Exchange exchange) {
		//create the more advanced CoapExchange object
		final CoapExchange ex = new CoapExchange(exchange,this);

  
		final Request req =exchange.getRequest();
		if(!req.getOptions().hasObserve()){
			//accept the request with an ack. the real response will come later
			//not done with observing because of copper bug
			ex.accept();
		}
		Response res = resClient.handleRequest(req);
		ex.respond(res);

	}

	private class ObserveMapping {
		public final List<ObserveRelation> endpointObservers;
		public final Future<?> observeResourceTask;

		public ObserveMapping(List<ObserveRelation> endpointObservers,
				Future<?> observeResourceTask){
			this.endpointObservers = endpointObservers;
			this.observeResourceTask = observeResourceTask;
		}
	}
	
}
