package it.unipi.ing.networkingpp;

import org.eclipse.californium.core.CoapClient;
import org.eclipse.californium.core.CoapResource;
import org.eclipse.californium.core.server.resources.Resource;
import org.eclipse.californium.core.WebLink;
import org.eclipse.californium.core.server.resources.CoapExchange;


import java.util.Arrays;
import java.util.List;
import java.util.ArrayList;
import java.util.LinkedList;

import java.net.URI;
import java.net.URISyntaxException;

public class ServerResource extends CoapResource {
	

	public ServerResource(String URI){
		super(URI.replace("[","").replace("]",""));


		ResourceDiscovery discovery = new ResourceDiscovery(this,URI);

		Resource well_known = new CoapResource(".well-known");
		well_known.add(discovery);
		this.add(well_known);

	}
}
