package it.unipi.ing.networkingpp;

interface NotificationHandler{
	public void onReceived();
	public void onError();
}
