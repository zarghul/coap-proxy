package it.unipi.ing.networkingpp;

import org.eclipse.californium.core.CoapServer;

import org.eclipse.californium.core.coap.CoAP.Type;
import org.eclipse.californium.core.coap.Response;
import org.eclipse.californium.core.coap.Request;

import org.eclipse.californium.core.CoapResource;
import org.eclipse.californium.core.server.resources.CoapExchange;

import java.util.logging.Level;
import java.util.logging.Logger;

public class ReverseProxyServer extends CoapServer {
	
	private static final Logger LOGGER = Logger.getLogger(ReverseProxyServer.class.getCanonicalName());

	public ReverseProxyServer(String border_router){

		CoapResource root = (CoapResource)this.getRoot();
		CoapResource wellKnown = (CoapResource)root.remove(".well-known");
		wellKnown.remove("core");
		wellKnown.add(new ServerDiscovery(root,
					"http://["+border_router+"]",0));
		root.add(wellKnown);

		LOGGER.log(Level.INFO,"Server started.");
	}

	public static void main(String[] args) {
		if(args.length<2)
		{
			System.out.println("MISSING ARGUMENT: border router address");
		}
		CoapServer server = new ReverseProxyServer(args[1]);
		server.start();
	}
}
