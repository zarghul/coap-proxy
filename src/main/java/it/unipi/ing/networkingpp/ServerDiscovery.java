package it.unipi.ing.networkingpp;

import org.eclipse.californium.core.CoapClient;

import org.eclipse.californium.core.CoapResource;

import org.eclipse.californium.core.server.resources.DiscoveryResource;
import org.eclipse.californium.core.server.resources.Resource;
import org.eclipse.californium.core.server.resources.CoapExchange;

import java.util.Timer;
import java.util.TimerTask;

import java.util.LinkedList;
import java.util.List;
import java.util.Iterator;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import java.util.concurrent.Executors;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.Callable;

import java.util.logging.Level;
import java.util.logging.Logger;

public class ServerDiscovery extends DiscoveryResource {
	
	private static final Logger LOGGER = Logger.getLogger(ServerDiscovery.class.getCanonicalName());

	private final DiscoveryTask discovery;
	private Resource root;

	private List<String> servers;

	public ServerDiscovery(Resource root,String borderRouter, int frequency) {
		super(root);
		this.root = root;

		this.discovery = new DiscoveryTask(borderRouter);

		servers = new LinkedList<String>();

		//TODO proper deletion
		if(frequency>0){
			Timer timer = new Timer();
			timer.schedule(discovery, 0, frequency);
		}
	}

	private class DiscoveryTask extends TimerTask {
		private final String borderRouter;
	
		public DiscoveryTask(String borderRouter){
			this.borderRouter = borderRouter;
		}

		@Override
		public void run(){
			
			List<String> all_nodes = getAllNodes(borderRouter);
			List<String> coap_nodes = getCoapNodes(all_nodes);

			List<String> toAdd = new LinkedList(coap_nodes);
			toAdd.removeAll(servers);
			List<String> toRemove = new LinkedList(servers);
			toRemove.removeAll(coap_nodes);

			for(String s: toAdd){
				LOGGER.info("New Coap Server: "+s);
				root.add(new ServerResource(s));
			}
			for(String s: toRemove){
				LOGGER.info("Removed Coap Server: "+s);
				CoapResource r = (CoapResource)root.getChild(s.replace("[","").replace("]",""));
				removeResourceTree(r);
			}

			servers = coap_nodes;
		}
	}

	//recursively delete the resource and all children
	private void removeResourceTree(CoapResource root){
		for(Resource r : root.getChildren()){
			removeResourceTree((CoapResource)r);
		}

		root.delete();
	}

	private List<String> getAllNodes(String url){
		List<String> list =  new LinkedList<String>();
		try{
			HttpClient client = new DefaultHttpClient();
			HttpGet request = new HttpGet(url);
			HttpResponse response = client.execute(request);
			BufferedReader rd = new BufferedReader
				  (new InputStreamReader(response.getEntity().getContent()));
			    
			

			String start = "</pre>Routes<pre>";
			String stop = "</pre></body></html>";
			String line = "";
			while (!(line = rd.readLine()).matches(start+".*")) {
			} 
			line = line.replace(start,"");
			do{
				list.add("["+line.split("/")[0]+"]");
			}while (!(line = rd.readLine()).matches(stop)); 

			response.getEntity().consumeContent();

		}catch(Exception e){
			e.printStackTrace();
		}

		return list;	
	}

	private List<String> getCoapNodes(List<String> addresses){
		ExecutorService executor = Executors.newCachedThreadPool();

		List<Future<String>> servers = new LinkedList<Future<String>>();
		List<String> result = new LinkedList<String>();
		for(final String a: addresses){
			servers.add(executor.submit(new Callable<String>(){
				@Override
				public String call(){
						CoapClient c = createClient(a);
						return c.ping() ? c.getURI() : null;
				}
			}));
		}
		for(Future<String> server: servers){
			try{
				String c = null;
				if((c = server.get())!=null){
					result.add(c);
				}
			}catch(Exception e){
			}
		}
		
		return result;
	}

	@Override 
	public void handleGET(CoapExchange exchange){
		discovery.run();
		super.handleGET(exchange);	
	}
}
